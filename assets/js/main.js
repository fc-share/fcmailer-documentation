/*
 * FCMailer - 2017
 * Documentation
 *
 * ScriptName: main.js
 * Version: 3.0
 *
 * FC-Dev team
 * http://foodconnection.jp/
 *
 * Email:		trung.styles@gmail.com
 *
 */

$(document).ready(function() {
	$.SyntaxHighlighter.loadedExtras = true;
	$.SyntaxHighlighter.init();
});